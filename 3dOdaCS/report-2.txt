
REFEREE REPORT:


Title: Smooth centrally symmetric polytopes in dimension 3 are IDP



Authors: Beck, Haase, Higashitani, Hofscheier, Jochemko, Katthan, Michalek




Journal: Annals of Combinatorics




SUMMARY: 

An open problem due to Oda is to determine if every smooth lattice polytope is IDP. This is a deep and difficult problem which has attracted a lot of attention from geometric combinatorialists and algebraic geometers. In this note, the authors prove that smooth centrally symmetric polytopes are IDP when restricted to dimension 3. This result is a nice contribution to the literature.


However, there appears to be one mathematical gap in their argument, in that the proof of Lemma 3.2 appears to be incomplete.  This certainly must be addressed before a recommendation in favor of publication can be made.  If this is fixed, and the other comments I make below are also addressed via revisions, then at that time I can strongly recommend that this paper be published.




COMMENTS:


1. Proof of Lemma 2.3: Please provide more detail in this proof. In particular, explain that all but one of the P-edge directions from each vertex of F lie in F, and thus the edge adjacent to v in F but not contained in F must be at lattice distance 1 from the affine hull of F in order for the smooth condition to hold.



2. Proof of Lemma 3.1: Instead of "south" and "west", please use "up" "down" "left" and "right" language, and indicate that this is relative to Figure 2.



3. Proof of Lemma 3.2: As currently written, the assumption that P is centrally symmetric is never used in this proof. For a general smooth 3-polytope P, I do not see why F' and F must have the same interior angles by construction, so I am assuming that there is an implicit use of the centrally symmetric condition in this part of the argument. It needs to be made clear, because the current version of the proof appears incomplete.



4. You should include several examples of smooth centrally-symmetric 3-polytopes, especially a couple examples that have a unimodular facet. 



5. Page 1, paragraph about IDP: you should state as theorems, and include references, the fact that unimodular simplices, parallelepipeds, and zonotopes are IDP. This fact is crucial for your later arguments.



6. Proof of Thm 1.3: You should add an explicit description of conv(-D,D) as a lattice parallelepiped, e.g. explicitly describe the lattice segments in the Minkowski sum decomposition of this object.
